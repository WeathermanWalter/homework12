<!doctype html>
<?php
require('mlib_functions.php');
require('mlib_values.php');
html_head("mlib release");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit'])) {
    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>

<h2>Release Equipment</h2>
<form action="mlib_release.php" method="post">
    <!-- display checked out equipment -->
    <table border=1>
        <tr>
            <td>Click to Release</td>
            <td>Title</td>
            <td>Type</td>
            <td>Description</td>
            <td>User</td>
            <td>Reserved Till</td>
        </tr>
<?php
    $results = $db->query("SELECT * FROM media WHERE status = 'active' AND user_id > 0");
    foreach ($results as $row) {
        echo "<tr>";
        echo "<td><input type='checkbox' name='id[]' value=".$row['id']."></td>";
        echo "<td>".$row['title']."</td>";
        echo "<td>".$row['type']."</td>";
        echo "<td>".$row['description']."</td>";
        $user_id = $row['user_id'];
        $results = $db->query("SELECT * FROM mlib_users WHERE id = $user_id")->fetch();
        $user_name = $results['first']." ".$results['last'];
        echo "<td>".$user_name."</td>";
        echo "<td>".$row['date_in']."</td>";
        echo "</tr>";
    }
?>
    </table>
    <input type="submit" name="submit" value="Submit"/><br/>
</form>

<?php $db = NULL ?><!-- closing off database -->
<?php
    } catch(PDOException $e) {
        echo 'Exception : '.$e->getMessage().'<br/>';
        $db = NULL;
    }
} else {
    //gathering variables
    $id = $_POST['id'];

    //opening database
    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $n = count($id);
        if ($n == 0) {
            echo "No items were selected.<br/>";
        } else {
            //TODO update database
            for ($i=0; $i < $n; $i++) {
                $db->exec("UPDATE media SET user_id = 0, date_in = NULL WHERE id = $id[$i]");
            }

            echo "<h1>Released the Following media</h1>";

            //print out a HTML table
            echo "<table border=1>";
            echo "<tr>";
            echo "<td>Title</td>";
            echo "<td>Type</td>";
            echo "<td>Description</td>";
            echo "<td>date due</td>";
            echo "</tr>";
            for ($i=0; $i < $n; $i++) {
                $sql = "SELECT * FROM media WHERE id = $id[$i]";
                $row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
                echo "<tr>";
                echo "<td>".$row['title']."</td>";
                echo "<td>".$row['type']."</td>";
                echo "<td>".$row['description']."</td>";
                echo "<td>".$row['date_in']."</td>";
                echo "</tr>";
            }
            echo "</table>";
        }

        //closing database
        $db = NULL;
    } catch(PDOException $e) {
        echo 'Exception : '.$e->getMessage().'<br/>';
        $db = NULL;
    }
}
require('mlib_footer.php');
?>
