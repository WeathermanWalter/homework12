<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib skeleton");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

# Code for your web page follows.
if (!isset($_POST['submit']))
{
    //opening database
    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //getting day 1 week ahead
        $results = $db->query("SELECT CURDATE() + INTERVAL 1 WEEK")->fetch();
        $next_week = $results[0];
?>
    <h2>Reserve Media</h2>
    <form action="mlib_reserve.php" method="post">
        Checked out to:
        <select name="user">
<?php
    //display users
    $results = $db->query('SELECT * FROM mlib_users');
    foreach ($results as $row) {
        echo "<option value=".$row['id'].">".$row['first']."</option>";
    }
?>
        </select><br/>
        <!--prompting for return date, default is 1 week from today-->
        Reserve Till (yyyy-mm-dd):
        <?php echo "<input type 'text' name='date_in' value='$next_week' /><br/>";?>
        <!--display media -->
        <table border=1>
            <tr>
                <td>Click to Reserve</td>
                <td>Media</td>
                <td>Type</td>
                <td>Description</td>
            </tr>
<?php
    $results = $db->query("SELECT * FROM media WHERE status = 'active' AND user_id = 0");
    foreach ($results as $row) {
        echo "<tr>";
        echo "<td><input type='checkbox' name='id[]' value=".$row['id']."></td>";
        echo "<td>".$row['title']."</td>";
        echo "<td>".$row['type']."</td>";
        echo "<td>".$row['description']."</td>";
        echo "</tr>";
    }
?>

        </table>
        <!-- submit button -->
        <input type="submit" name="submit" value="Submit"/><br/>
    </form>
    <?php $db = NULL; ?> <!-- closing database connection -->
<?php
    } catch (PDOException $e) {
        echo 'Exception : '.$e->getMessage().'<br/>';
        $db = NULL;
    }
} else {
    //getting variables
    $id = $_POST['id'];
    $user = $_POST['user'];
    $date_in = $_POST['date_in'];

    //initilizing the database
    try {
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //fetching name of user
        $results = $db->query("SELECT * FROM mlib_users WHERE id = $user")->fetch();
        $user_name = $results['first']." ". $results['last'];

        //validate date_in
        if (!MyCheckDate($date_in)) {
            try_again("Entered the wrong date, Format is yyyy-mm-dd.");
        }

        //get todays date
        $results = $db->query("SELECT CURDATE()")->fetch();
        $today = $results[0];

        //check for date in the past
        if ($date_in < $today) {
            try_again("Enter a date that is in the future");
        }

        //check for valid input
        $n = count($id);
        if ($n == 0) {
            echo "You did not choose any items<br/>";
        } else {
            //TODO update media
            for ($i=0; $i < $n; $i++) {
                $db->exec("UPDATE media SET user_id = $user, date_in = '$date_in' WHERE id = $id[$i]");
            }

            //show HTML table
            echo "<h1>Media Reserved</h1>";
            echo "<h2>Your media is due by $date_in</h2>";

            echo "<table border = 1>";
            echo "<tr>";
            echo "<td>Media</td>";
            echo "<td>Type</td>";
            echo "<td>Description</td>";
            echo "<td>User</td>";
            echo "<td>Reserved Until</td>";
            echo "</tr>";

            for($i = 0; $i < $n; $i++) {
                $sql = "SELECT * FROM media WHERE id = $id[$i]";
                $row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
                echo "<tr>";
                echo "<td>".$row['title']."</td>";
                echo "<td>".$row['type']."</td>";
                echo "<td>".$row['description']."</td>";
                echo "<td>".$user_name."</td>";
                echo "<td>".$row['date_in']."</td>";
                echo "</tr>";
            }
            echo "</table>";

            $db = NULL;
        }
    } catch (PDOException $e) {
        echo 'Exception : '.$e->getMessage().'<br/>';
        $db = NULL;
    }
}
require('mlib_footer.php');
?>
