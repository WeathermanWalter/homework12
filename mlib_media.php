<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib media");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');
require('mlib_values.php');

if (we_are_not_admin()) {
    exit;
}

try {
  $db = new PDO(DB_PATH,DB_LOGIN,DB_PW);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  //Test if the database is up
  $test = $db->query("SELECT * FROM mlib_types");
} catch(PDOException $e) {
  echo ("Could not open database<br>");

  echo 'Exception : '.$e->getMessage();
  $db = null;
  exit(1);
}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <!-- Display a form to capture information -->
  <h2>Add Media</h2>
  <form action="mlib_media.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>Title</td>
        <td align="left"><input type="text" name="title" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Author</td>
        <td align="left"><input type="text" name="author" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Description</td>
        <td align="left"><input type="text" name="description" size="70" maxlength="70"></td>
      </tr>

      <tr>
        <td>Type</td>
        <td align="left">
            <select name="type">
            <?php
                $results = $db->query('SELECT * FROM mlib_types');
                foreach ($results as $row) {
                    echo "<option value=".$row['type'].">".$row['type']."</option>";
                }
            ?>
            </select>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>
<?php
} else {
  # Process the information from the form displayed
  //$yourfield = $_POST['yourfield'];
  $title = $_POST['title'];
  $author = $_POST['author'];
  $type = $_POST['type'];
  $description = $_POST['description'];

  //validation
  $errors = validate_media($title, $author, $type, $description);
  if(!empty($errors)) {
    try_again("Detected errors: <br/>" . str_replace(".", ".<br/>", implode($errors)));
  }

  //putting the media in the database
  try {
    $db->exec("INSERT INTO media(title, author, description, type, user_id, status) VALUES ('$title','$author','$description','$type',0, 'active');");
} catch(PDOException $e) {
    echo "Could not insert data into database";
    try_again("");
}

  print "<h2>Media Added</h2>";
  print "<table border=1>";
  print "<tr>";
  print "<td>Title</td>";
  print "<td>Author</td>";
  print "<td>Description</td>";
  print "<td>Type</td>";
  print "</tr>";
  print "<tr>";
  print "<td>".$title."</td>";
  print "<td>".$author."</td>";
  print "<td>".$description."</td>";
  print "<td>".$type."</td>";
  print "</tr>";
  print "</table>";
}
require('mlib_footer.php');
?>
