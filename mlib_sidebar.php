<aside id="sidebar">
  <nav>
      <ul>
        <li>
          <a href="mlib_login.php"> Login</a><br/>
        </li>
      </ul>
    <hr/>
    <ul>
      <li>
        <a href="mlib_status.php"> Status </a><br/>
      </li>
    </ul>
    <hr/>
    <ul>
      <li>
        <a href="mlib_reserve.php"> Reserve</a><br/>
      </li>
    </ul>
    <hr/>
    <ul>
      <li>
        <a href="mlib_release.php"> Release</a><br/>
      </li>
    </ul>
    <?php
    if (!empty($_SESSION['valid_user'])) {
    ?>
    <hr/>
    <!-- admin only options -->
    <h5>--ADMIN--</h5>
    <hr/>
    <ul>
      <li>
        <a href="mlib_administrator.php"> Admin</a><br/>
      </li>
    </ul>
  <hr/>

    <ul>
      <li>
        <a href="mlib_media.php"> Media</a><br/>
      </li>
    </ul>
    <hr/>

    <ul>
      <li>
        <a href="mlib_upload.php"> Upload</a><br/>
      </li>
    </ul>
    <hr/>

    <ul>
      <li>
        <a href="mlib_logout.php"> Logout</a><br/>
      </li>
    </ul>
    <hr/>

    <ul>
      <li>
        <a href="mlib_users.php"> Users</a><br/>
      </li>
    </ul>
    <hr/>
    <?php
    }
    ?>
  </nav>
</aside>
