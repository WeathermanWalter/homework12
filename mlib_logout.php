<!doctype html>
<?php
session_start();
require('mlib_functions.php');
html_head("mlib logout");
require('mlib_header.php');

$old_user = $_SESSION['valid_user'];
unset($_SESSION['valid_user']);
session_destroy();

require('mlib_sidebar.php');
echo "<h1>Log Out</h1>";

if(empty($old_user)) {
    echo "Already logged out<br/>";
} else {
    echo "Logged out<br/>";
}

require('mlib_footer.php');
?>
