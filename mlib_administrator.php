<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib administrator");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
    exit;
}

if (!isset($_POST['submit']))
{
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>

    <h2>Change Administration Priviledges</h2>
    <form action="mlib_administrator.php" method="post">
      <!-- display types -->
      <table border=1>
        <tr>
          <td>Click one to Change</td><td>User</td><td>Login</td>
        </tr>

<?php
    $results = $db->query("SELECT * FROM mlib_users");
    foreach($results as $row)
    {
        echo "<tr>";
        echo "<td><input type='radio' name='id' value=".$row['id']."></td>";
        echo "<td>".$row['first']." ".$row['last']."</td>";
        echo "<td>".$row['login']."</td>";
        echo "</tr>";
    }
?>
      </table>
      <p>Clicking an entry with a login will remove administration privileges.</p>
      <p>Clicking an entry without a login will enable administration privileges. Enter login and password below:</p>
      Login: <input type="text" name="login"/><br/>
      Password: <input type="text" name="password"/><br/>
      <input type="submit" name="submit" value = "Submit"/><br/>
    </form>

<?php

    // close the database connection
    $db = NULL;
  } catch(PDOException $e) {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
    }
} else {
?>

<h2>Administration Priviledges Changed</h2>

<?php
$id = $_POST['id'];
$login = $_POST['login'];
$password = $_POST['password'];

try {
    if (empty($id)) {
        echo "You did not select any users to change privileges.<br/>";
    } else {
        //open the database
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //update user as appropriate
        $results = $db->query("SELECT * FROM mlib_users where id = $id")->fetch(PDO::FETCH_ASSOC);
        if (empty($results['login'])) {
            //set the login and password to enable administrator privileges
            //clean up and validate data
            $login = trim($login);
        if (empty($login)) {
            try_again("Login cannot be empty.");
        }
        $password = trim($password);
        if (strlen($password) < 8) {
            try_again("Password must be at least 8 characters.");
        }
        //change password into a sha1 hash
        $passwd = sha1($password);
        //check for duplicate login
        $results = $db->query("SELECT count(*) FROM mlib_users WHERE login = '$login'")->fetch(); //count the number of entries with the name
        if ($results[0] > 0) {
            try_again($login." is not unique. Logins must be unique.");
        }
        $db->exec("UPDATE mlib_users SET login = '$login', password = '$passwd' WHERE id = $id");
      } else {
        //remove the login and password
        $db->exec("UPDATE mlib_users SET login = NULL, password = NULL   WHERE id = $id");
      }

    //now output the data to a simple html table...
    echo "<table border=1>";
    echo "<tr>";
    echo "<td>User</td><td>Login</td>";
    echo "</tr>";
    $sql = "SELECT * FROM mlib_users where id = $id";
    $results = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
    echo "<tr>";
    echo "<td>".$results['first']." ".$results['last']."</td><td>".$results['login']."</td>";
    echo "</tr>";
    echo "</table>";
    }

    // close the database connection
    $db = NULL;
} catch(PDOException $e) {
        echo 'Exception : '.$e->getMessage().'<br/>';
        $db = NULL;
    }
}
require('mlib_footer.php');
?>
