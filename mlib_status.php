<?php
require('mlib_functions.php');
html_head("mlib status");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');
require('mlib_values.php');
try {
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $test = $db->query("SELECT * FROM mlib_types");
} catch (PDOException $e) {
    echo "Could not open database";
    echo $e->getMessage();
    $db =  null;

    require('mlib_footer.php');
    exit(0);
}

print "<h2>listing all media</h2>";

print "<table border = 1>";
print "<tr>";
print "<th>Title</th>";
print "<th>Author</th>";
print "<th>Description</th>";
print "<th>Type</th>";
print "<th>Status</th>";
print "<th>ID</th>";
print "</tr>";

$results = $db->query("SELECT * FROM media;");
foreach($results as $row) {
    print "<tr>";
    print "<td>".$row['title']."</td>";
    print "<td>".$row['author']."</td>";
    print "<td>".$row['description']."</td>";
    print "<td>".$row['type']."</td>";
    print "<td>".$row['status']."</td>";

    $user_id = $row['user_id'];
    if ($user_id != 0) {
        $u_result = $db->query("SELECT * FROM mlib_users WHERE id = $user_id;")->fetch();
        $user_name = $u_result['first']." ".$u_result['last'];
    } else {
        $user_name = "available";
    }
    print "<td>".$user_name."</td>";

    print "</tr>";
}
print "</table>";



$db = null;
require('mlib_footer.php');
?>
