<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib proccessing media");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');
require('mlib_values.php');

# Code for your web page follows.
echo "<h2>Proccessing Media</h2>";

//check for file error
if($_FILES['userfile']['error']) {
    echo 'Problem: ';
    switch ($_FILES['userfile']['error']) {
      case 1:  echo 'File exceeded upload_max_filesize';  break;
      case 2:  echo 'File exceeded max_file_size';  break;
      case 3:  echo 'File only partially uploaded';  break;
      case 4:  echo 'No file uploaded';  break;
    }
    exit;
}

//is the file a txt file?
if ($_FILES['userfile']['type'] != 'text/plain') {
  echo 'Problem: file is not plain text';
  exit;
}

//putting the file in the uploads folder
$upfile = './uploads/'.$_FILES['userfile']['name'];

if (is_uploaded_file($_FILES['userfile']['tmp_name']))
{
  if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $upfile))
  {
    echo 'Problem: Could not move file to destination directory';
    exit;
  }
}
else
{
  echo 'Problem: Possible file upload attack. Filename: ';
  echo $_FILES['userfile']['name'];
  exit;
}

echo "File uploaded succesfully<br><br>";

//opening file
$fp = fopen($upfile, 'r');
if (!$fp) {
    echo "<p>could not open $upfile</p>";
    exit;
}

//read data from file
while(!feof($fp)) {
    $line = fgets($fp);
    $line_array = explode(',', $line);
    $title = trim($line_array[0]);
    $author = trim($line_array[1]);
    $type = trim($line_array[2]);
    $description = trim($line_array[3]);

    //validation
    $errors = validate_media($title, $author, $type, $description);
    if(empty($errors)) {
        //put into database
        $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->exec("INSERT INTO media (title, author, type, description, status) VALUES ('$title', '$author', '$type', '$description', 'active');");
        /*TODO validate $db code below
        $db->exec("INSERT INTO media(title, author, description, type, user_id, status) VALUES ('$title','$author','$description','$type',0, 'active');");
        */

        echo "Success, added $title to database<br/>";

    } else {
        echo "<p1>Found some errors:</p1><br/>";
        foreach($errors as $error) {
          echo $error."<br/>";
        }
    }
    //printing data to user
    echo "Title: $title<br/>";
    echo "Author: $author<br/>";
    echo "Type: $type<br/>";
    echo "Description: $description<br/> <br/>";
}
fclose($fp);

require('mlib_footer.php');
?>
