<!doctype html>
<?php
require('mlib_functions.php');
html_head("mlib users");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
  <h2>Add User</h2>
  <form action="mlib_users.php" method="post">
    <table border="0">
      <tr bgcolor="#cccccc">
        <td width="100">Field</td>
        <td width="300">Value</td>
      </tr>
      <tr>
        <td>First</td>
        <td align="left"><input type="text" name="first" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Last</td>
        <td align="left"><input type="text" name="last" size="35" maxlength="35"></td>
      </tr>
      <tr>
        <td>Email</td>
        <td align="left"><input type="text" name="email" size="70" maxlength="70"></td>
      </tr>
      <tr>
        <td colspan="2" align="right"><input type="submit" name="submit" value="Submit"></td>
      </tr>
    </table>
  </form>

<?php
} else {

//validate the input and add to the database

$first = $_POST['first'];
$last = $_POST['last'];
$email = $_POST['email'];

try {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //clean up and validate data
    $first = trim($first);
    if (empty($first)) {
        try_again("First name cannot be empty.");
    }
    $last = trim($last);
    if (empty($last)) {
        try_again("Last name cannot be empty.");
    }
    $email = trim($email);
    if (empty($email)) {
        try_again("Email cannot be empty.");
    }

    //check for duplicate user name
    $sql = "SELECT count(*) FROM mlib_users WHERE first = '$first' AND last = '$last'";
    $results = $db->query($sql)->fetch();
    if ( $results[0] > 0) {
        try_again($first." ".$last." is not unique. Names must be unique.");
    }

    //check for duplicate email
    $sql = "SELECT count(*) FROM mlib_users WHERE email = '$email'";
    $results = $db->query($sql)->fetch(); //count the number of entries with the email
    if ( $results[0] > 0) {
        try_again("$email is not unique. Email must be unique.");
    }

    //insert data...
    $db->exec("INSERT INTO mlib_users (first, last, email) VALUES ('$first', '$last', '$email');");
    //oh, i get it, ; is used for executing, and fetch will 'fetch' the data

    //get the last id value inserted into the table
    $last_id = $db->lastInsertId();

    //now output the data from the insert to a simple html table...
    echo "<h2>User Added</h2>";
    echo "<table border=1>";
    echo "<tr>";
    echo "<td>Id</td><td>First</td><td>Last</td><td>Email</td>";
    echo "</tr>";
    $row = $db->query("SELECT * FROM mlib_users where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
    echo "<tr>";
    echo "<td>".$row['id']."</td>";
    echo "<td>".$row['first']."</td>";
    echo "<td>".$row['last']."</td>";
    echo "<td>".$row['email']."</td>";
    echo "</tr>";
    echo "</table>";

    //close the database connection
    $db = NULL;

} catch(PDOException $e) {
        echo 'Exception : '.$e->getMessage().'<br/>';
        $db = NULL;
    }
}
require('mlib_footer.php');
?>
